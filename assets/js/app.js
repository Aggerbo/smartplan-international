function updateChart(){
  //Get context with jQuery - using jQuery's .get() method.
  var ctx = $("#myChart").get(0).getContext("2d");
  //This will get the first returned node in the jQuery collection.
  var myNewChart = new Chart(ctx);
  var data = [{
    value:$("#myChart").data("unmanned"),
    color: "#C94825"
  },
  {
    value:$("#myChart").data("manned"),
    color: "#579c22"
  }]
  new Chart(ctx).Doughnut(data);
}
$(document).ready(function(){


  $('#focusCompany').on('click', function(){
    $('body').scrollTop(0);
    $('#accountName').focus();
    $('#accountName').addClass('animated bounce');
  });

  $('#signupForm').isHappy({
    fields: {
      // reference the field you're talking about, probably by `id`
      // but you could certainly do $('[name=name]') as well.
      '#accountName': {
        required: true,
        message: $("#accountName").data("err")
      },
      '#email': {
        required: true,
        message: $("#email").data("err"),
        test: function(email){
            var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            return re.test(email);
        }
    },
    '#password':{
        required: true,
        message: $("#password").data("err")
    }
    }
  });

  // var actionUrl = $("form").attr("action");
  // $("form").attr("action", actionUrl+"?referrer="+document.referrer);
  // $(".try-btn").each(function(){
  //   var url = $(this).attr("href")
  //   var newUrl = url+"?referrer="+document.referrer;
  //   $(this).attr("href", newUrl)
  // })
  $(".go-to").click(function(){
    var dest = $("a", $(this)).data("go-to")
    $("body, html").animate({scrollTop:$("#"+dest).offset().top}, '500', 'swing', function() {

    });
  })
  $("input").placeholder()
  $(".q-btn, .supportBtn").on("click", function(){
    $("#contact-form").modal();
  })
  if ($(".excel, .excel-upload-widget").length){
    $(".upload, .uploadTextLink").click(function(){
      $("#upload-excel").modal()
    })
  }
  if ($(".fairbar").length){
    $(".play").click(function(){
      $("#video-popup").modal()
    })
  }
  $(".big-signup-btn, .menu-signup").on("click", function(e){
    e.preventDefault();
    ga('send', "event", "bigSignupButton", "click", "signup button", {
      "hitCallback": function(){
        window.location.href="https://smartplanapp.io/get-started/?referrer="+document.referrer;
      }
    });
  })
  //######################Blog Functions #####################
  if ($("#blog-sales .blog-posts").length > 0){

    myJsonpCallback = function(data)
    {
        var posts = data.response.posts;
        posts.reverse()
        for (var i = posts.length - 1; i >= 0; i--) {
          var id = posts[i].id
          $(".blog-posts").append("<div class='post' id="+id+"/>")
          $("#"+id).append("<h1>"+posts[i].title+"</h1>")
          var dateList = posts[i].date.split(" ")
          var date = new Date(dateList[0]+"T"+dateList[1])
          var dateString = date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
          $("#"+id).append("<p class='date'>Udgivet d. "+dateString+"</p>")
          $("#"+id).append(posts[i].body)
        };
    }

    $.ajax({
        type: "GET",
        url : "https://api.tumblr.com/v2/blog/smartplanhq.tumblr.com/posts/",
        dataType: "jsonp",
        data: {
            api_key : "7K0gT5s4NoCM3AwO2iWp3TwqQ60Jsx3xcncnPiMTCzk66HzlXU",
            tag: "news",
            limit: 10,
            jsonp : "myJsonpCallback"
        }
    });
  }
  //IF SECTION MAGIC ER TIL STEDE:

  if ($(".chart-con").length > 0){
    updateChart()
    var sectionPosition = $(".chart-con").offset().top
    var animated = false;
    $(window).on("scroll", function(){
      if($(document).scrollTop() >= sectionPosition-300){
        $(window).off("scroll");
        updateChart()
      }
    })
  }

  //######################SHIFTPLAN Functions #####################
  var t;
  function shuffleFilter(timer){
      t = setInterval(function(){
        if ($(".filter li.selected").data("job-type") == 3){
          $(".filter .all").click();
        }
        else{
          $(".filter li.selected").next().click()
        }
      }, 2000)

  }
  $(".shift-plan").on("click", ".filter li", function(e){

    //clearInterval(t)
  	//$(".shift.dimmed").toggleClass("dimmed")
  	if ($(this).hasClass("all")){
  		$(".filter .selected").removeClass("selected")
  		$(this).addClass("selected")
  		$(".dimmed").removeClass("dimmed")
  	}
  	else {
  		var type = $(this).data("job-type")
  		var for_sale = $(this).data("for-sale")
  		var my_shifts = $(this).data("my-shift")
  		if ($(this).hasClass("selected")){
  			$(".shift, .time-block").removeClass("dimmed")
  			$(".filter li").removeClass("selected")
  			$("li.all").addClass("selected")
  		}
  		else{
  			$(".filter .selected").removeClass("selected")
  			$(".dimmed").removeClass("dimmed")

  			if (for_sale){
  				$(".filter li[data-for-sale='"+for_sale+"']").addClass("selected")
  				$(".shift:not([data-for-sale='"+for_sale+"'])").addClass("dimmed")
  			}
  			else if (my_shifts){
  				$(".filter li[data-my-shift='"+my_shifts+"']").addClass("selected")
  				$(".shift:not([data-my-shift='"+my_shifts+"'])").addClass("dimmed")
  			}
  			else{
  				$(".filter li[data-job-type='"+type+"']").addClass("selected")
  				$(".shift:not([data-job-type='"+type+"'])").addClass("dimmed")
  			}

  		}
  	}
  	$(".shift-container").each(function(){
  		var number_of_shifts = $(".shift", $(this))
  		var number_of_dimmed = $(".shift.dimmed", $(this))
  		if (number_of_shifts.length == number_of_dimmed.length){
  			var block = $(this).prev()
  			block.addClass("dimmed")
  		}
  	})

  })
    $(".shift-plan").on("click", ".filter", function(e){

          if(e.which) {
              clearInterval(t)
          }
          else {
              //Triggered by code
          }

    })
  shuffleFilter()
  // ################# Wish slider ##################### //
  $(".wish-expl").on("click", "#charlotte-bio", function(){
    $("#charlotte-screen").removeClass("bounceOutRight")
    $("#michael-bio").removeClass("selected")
    if ($("#charlotte-bio").hasClass("selected")){

    }
    else{
      //$("#charlotte-screen").addClass("visible")
      $("#michael-screen").addClass("bounceOutRight")
      $("#charlotte-screen").addClass("bounceInRight")
      $('#charlotte-screen').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(".bounceInRight").removeClass("bounceInRight")
        //$(".bounceOutRight").removeClass("bounceOutRight")
      });
      $(this).addClass("selected")
    }


  })
  $(".wish-expl").on("click", "#michael-bio", function(){
    $("#michael-screen").removeClass("bounceOutRight")
    $("#charlotte-bio").removeClass("selected")

    if ($("#michael-bio").hasClass("selected")){

    }
    else{
      //$("#michael-screen").addClass("visible")
      $("#charlotte-screen").addClass("bounceOutRight")
      $("#michael-screen").addClass("bounceInRight")
      $('#michael-screen').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(".bounceInRight").removeClass("bounceInRight")
        //$(".bounceOutRight").removeClass("bounceOutRight")
      });

      $(this).addClass("selected")
    }


  })
// ################# Skiftet svg ud med png filer i IE ##################### //
  if(!Modernizr.svg) {
      $('img[src*="svg"]').attr('src', function() {
          return $(this).attr('src').replace('.svg', '.png');
      });
  }

// ################# Play video ##################### //
$(".goldHead").on("click", ".play_video", function(){
  $(".gold-video").addClass("show-video");
  $(".video-text").fadeOut();
  $('.gold-video').get(0).play();
});

  $('.goldHead').on("click", ".gold-video", function() {
    if ($(".video-text:visible").length == 0){
      if (this.paused == false) {
          this.pause();
      } else {
          this.play();
      }
    }
  });

$(".gold-video").bind("ended", function() {
  $(".gold-video").removeClass("show-video");
  $(".video-text").fadeIn();
});
// ################# Navigation hide/show ##################### //
  $("nav").on("click", ".nav-icon", function(){
    $(".nav-list").addClass("menu-on")
    if ($(".nav-icon").hasClass("close")){
      $(".nav-icon").removeClass("close")
      $(".nav-list").removeClass("menu-on")
    }
    else{
      $(".nav-icon").addClass("close")
    }

  })
  $(".nav-list").on("click", "li", function(){
    //$(".nav-list").hide();
    $(".nav-icon").removeClass("close")
  })

  // CAROUSEL ON LOGBOOK
  /*var t = setInterval(function(){
    console.log($(".circle").attr("class"))

  }, 1000)*/
  $(".carousel").on("mouseenter", ".des", function(){
    //clearInterval(t)
    $(".circle").removeClass("anno-2 anno-1 anno-3")
    $(".circle").addClass($(this).data("anno"))
    if ($(".circle").hasClass("anno-3")){
      $(".ipad-container img:first").hide()
      $(".ipad-container img:last").show()
    }
    else{
      $(".ipad-container img:first").show()
      $(".ipad-container img:last").hide()
    }
  })


    // Punch clock in and out selector
  $(".selector").on("click", ".clock-in", function(){
    $(".punch_in").addClass("fadeIn");
    $(".punch_in").show();
    $(".punch_out").hide();
    $(".clock-in").addClass("active");
    $(".clock-out").removeClass("active");
  })

  $(".selector").on("click", ".clock-out", function(){
    $(".punch_out").addClass("fadeIn");
    $(".punch_out").show();
    $(".punch_in").hide();
    $(".clock-out").addClass("active");
    $(".clock-in").removeClass("active");
  })
});
