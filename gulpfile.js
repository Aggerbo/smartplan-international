var gulp = require('gulp');
var l10n = require('gulp-l10n');
var watch = require("gulp-watch");

//var tap = require("gulp-tap");
//var insertLines = require('gulp-insert-lines');


// Prior to localization, pipe your locales to the setLocales method

gulp.task('load-locales', function () {
  return gulp.src('locales/*.json')
    .pipe(l10n.setLocales());
});

// Files piped to the plugin are localized and cloned to a separate subdirectory
// for each locale. e.g.: 'index.html' > 'de/index.html'

gulp.task('localize', ['load-locales'], function () {
  return gulp.src(['Build/**/*.html', '!Build/no/**/*.html'])
    .pipe(l10n())
    .pipe(gulp.dest('Build'))
});

var opts = {
  elements: ['title', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
  attributes: ['alt', 'title', "placeholder"],
  attributeSetter: 'translate-attrs'
};

gulp.task('extract-locales', function () {
  return gulp.src('Build/*.html')
    .pipe(l10n.extract(opts))
    .pipe(gulp.dest('locales'));
});

// gulp.task("watch", function(){
//     gulp.watch('Build/**/*.html', {readDelay:100}, ['localize'])
// })

//gulp.task('default', ['extract-locales']);
gulp.task('default', ['localize']);
